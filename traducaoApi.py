import requests
import json #biblioteca para reconhecer e depois formatar a resposta json das API's
frase = str(input('Digite uma frase para traduzir: '))
print('=-'*50,'\n')
ing = requests.post('https://api.mymemory.translated.net/get?q={}& langpair=pt|en'.format(frase))#envia a frase digitada para a API de tradução para inglês
dicionario = json.loads(ing.text)#variavel dicionario recebe a resposta em json da api
frase = dicionario['responseData']#a frase recebe a chave 'responseData' da variavel dicionario, que é a parte do codigo json que contem a tradução para o ingês 
dot = requests.get('https://api.funtranslations.com/translate/dothraki.json',"text={}".format(frase))#agora mandamos a frase que, que esta com a tradução em inglês, para a API do funtranslations.
dicionario2 = json.loads(dot.text)#A variavel dicionario2 recebe a resposta json da API
print(dicionario2['contents'])#Escreve na tela a tradução da frase para o dothraki mas tambem mostra a tradução em inglês 
print('=-'*50)