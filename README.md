# Teste Lógico Lab360

Oi, se você está lendo essa mensagem, é porque chegou até a ultima etapa do nosso processo seletivo \o/
Esse "teste" é criado pra saber o quanto você é familiar com algumas tecnologias que usamos, mas pode testar sua capacidade de aprendizagem se você não dominar algumas ferramentas.

## Git
Na Lab, todos os projetos utilizam GIT, que é um sistema de controle de versões. É amplamente utilizado no desenvolvimento de software, então provavelmente você vai usa-lo (ou já usou) alguma vez na vida.

Se você não souber nada de git, esse guia aqui pode te ajudar bastante: [http://rogerdudler.github.io/git-guide/index.pt_BR.html]

Agora que você já está familiar, precisamos que você crie um novo branch com o seu nome, pode ser apenas o primeiro. (ex: o meu seria "pedro"). Você vai fazer todo o código necessário nesse branch.

Lembre-se que cada commit funciona como um log do seu processo como programador, portanto atenção nos commits.

## NodeJS
Muito embora esse teste possa ser feito em qualquer linguagem, aqui na Lab nós trabalhamos bastante com NodeJS, que é uma linguagem bem simples, baseada na sintaxe do JavaScript. Não esperamos que você já domíne o NodeJS, e também por isso, disponibilizamos um curso super completo, online, caso você tenha interesse em aprender.

Entregar esse teste em Node conta **muitos** pontos pra você.

O curso está nesse link: [https://www.udemy.com/the-complete-nodejs-developer-course-2/learn/v4/content]
Pra acessar, use o meu e-mail: contato@pedrosantiago.com.br, e a senha: lab360

É um curso bem completo, mas depois de umas poucas horas você já vai ter nível pra fazer algumas coisas bem legais. 
Se você não estiver tranquilo com o inglês, ele tem legenda ;)

## APIs e REST
Último conceito antes de começarmos com a mão na massa. Mas é importante. Ao trabalhar com a gente você vai perceber que muitas das vezes nós em vez de desenvolver um sistema inteiro, apenas integramos duas coisas que já existem para funcionarem juntas. Isso porque nós não vamos reinventar a roda, portanto se algo já foi desenvolvido, é mais fácil e rápido que a gente apenas integre da forma como precisamos.

Pra que isso seja possível, tem que existir um meio de campo na integração. Normalmente nós utilizamos as APIs dos aplicativos que queremos integrar. Aos poucos, você vai ver que a grande maioria delas utiliza o padrão REST. 

De novo, vale uma lida se você quiser reforçar ou entender esses conceitos, eles vão ser úteis no decorrer do teste: [https://code.tutsplus.com/pt/tutorials/a-beginners-guide-to-http-and-rest--net-16340]

## Okay, adiante.

### Algumas perguntas e respostas
Ele tem prazo? 
R: sim, uma semana!

Eu preciso conhecer tudo o que foi mencionado acima? 
R: não! Se você conhecer tudo, ótimo, mas a ideia é justamente ver se você consegue aprender algo novo, então de certa forma é até melhor que não conheça tudo, mas que use esse teste pra aprender alguma coisa nova (vai ser util).

Quando eu terminar, o que devo fazer? 
R: `git push origin <seunome>` e depois um pull request pra mim no `master`. manda um e-mail pra gente avisando se for o caso. Vamos analisar rápido e prometemos não deixar você no escuro.

Se eu tiver alguma dúvida, posso entrar em contato?
R: sim e não. Tudo depende do seu bom senso, mas a ideia é que você tenha que resolver esses problemas no seu dia a dia, então mais vale um google bem dado #dica.

Tem alguma coisa que a gente deveria ter mencionado aqui e esqueceu?
R: se sim, escreve nesse README e ficaremos felizes em saber.

Quais são as regras do teste?
R: vale usar qualquer biblioteca, mas não vale usar SDKs da Microsoft (use o REST)

### Agora sim, o Teste:

O dothraki é uma língua artificial criada para os dothraki, Povo Nômade do Mar de Dothraki uma grande região no interior do continente de Essos , na série A Song of Ice and Fire (As Crônicas de Gelo e Fogo).
Dothraki é uma língua muito complexa. Muito embora tenha sido criada artificialmente, tem todas as estruturas gramaticais de uma língua como o português.
Nós gostamos de Dothraki.

Você vai criar um comando que traduza uma frase qualquer em portugues para o idioma Dothraki, para que nós possamos entender Game of Thrones corretamente.
Para isso, você vai usar a API do microsoft translator para traduzir a frase para inglês, em seguida pegar essa frase e traduzir para dothraki usando a API do FunTranslations.
Depois disso vai exibir pra gente a frase traduzida em Dothraki. 

A síntaxe do comando pode ser algo como `node traduzir.js "Olá mundo"` ou `node traduzir.js --frase="Eu sou um selvagem"`. 
Mais importante que essa síntaxe é funcionar adequadamente. 

EXTRA (opcional)
- Fazer a API traduzir outros idiomas, usando a opção '--language'


API da Microsoft para Tradução [https://docs.microsoft.com/pt-br/azure/cognitive-services/translator/translator-info-overview]
API do FunTranslations: [https://funtranslations.com/dothraki]

Dica: Vá por partes, comece usando a API do FunTranslations e faça do inglês para o Dothraki primeiro, é mais simples.

If yeri translate jinak sentence,  kisha will give yeri loy extra points.
Davra luck/Boa Sorte!

Equipe Lab360






